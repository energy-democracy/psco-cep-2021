### A Pluto.jl notebook ###
# v0.14.5

using Markdown
using InteractiveUtils


md"""
# This is a test


Hello and Welcome to the notebook, here we have defined a mathematical function
"""


# ╔═╡ eec729c0-a11c-45bc-8b27-05dceea1ee79
function factorial(n)
	if n == 0
		return 1
	else
		return n * factorial(n-1)
	end
end

md"And here we can check to see if it works!"

# ╔═╡ 075714fb-fa0e-425b-981a-f946e4a7813b
factorial.(1:10)

# ╔═╡ Cell order:
# ╠═eec729c0-a11c-45bc-8b27-05dceea1ee79
# ╠═075714fb-fa0e-425b-981a-f946e4a7813b
