### A Pluto.jl notebook ###
# v0.14.5

using Markdown
using InteractiveUtils

# ╔═╡ a053b143-0c44-4743-95b8-bd7dc2cc29eb
begin
	# Uncomment this line if you need to install these packages:
	# import Pkg; Pkg.add("JuMP"); Pkg.add("Clp"); Pkg.add("DataFrames"); Pkg.add("CSV"); Pkg.add("Statistics"); Pkg.add("Plots")
	using JuMP, Clp
	using Plots
	using DataFrames, CSV, Statistics
	ENV["COLUMNS"]=120 # Set so all columns of DataFrames and Matrices are displayed
end

# ╔═╡ 8db6b4c4-41c9-4a52-94d0-c4a6ac10a2c4
md"""# A Basic Capacity Expansion Model with regards to Denver CO

*by Jesse D. Jenkins and Michael R. Davidson (last updated: September 21, 2020) Adapted by Brad and Nicole Venner*


This notebook presents a basic electricity generation capacity expansion model: the task of minimizing the cost of fixed and variable costs across a fleet of generators to meet anticipated future electricity demand. 

We first present a basic optimization formulation of this optimal thermal generator capacity expansion problem, assuming a "greenfield" setting with no existing generating capacity. We also show how to create this model with Julia + JuMP.

Next, we discuss the impact of variable renewable energy sources (wind, solar) and present a slightly modified formulation that co-optimizes thermal and renewable capacities. Finally, we will note how this problem generalizes to a "brownfield" expansion problem including existing generators, with optimal retirement decisions included in the problem.

Note these capacity expansion problems will be basic/simplified formulations, ignoring inter-temporal operating constraints (e.g. ramp limits), energy storage, network constraints and geospatial tradeoffs, unit commitment decisions for thermal generators, and operating reserves. 

We will define a more complex model integrating these factors in a future notebook after familiarizing ourselves with operational models (economic dispatch, unit commitment, and optimal power flow).
"""

# ╔═╡ 236497a3-ab00-438e-8ad8-50af3c675bdb
md"""
## Optimal thermal generator expansion problem

Assume a utility can build any of the following four thermal generation types: geothermal, coal, natural gas combined cycle gas turbine (CCGT), and natural gas combustion turbine (CT). 

The utility must decide on the mix of thermal generators to meet forecasted demand in a future planning year across each hour of the year. Assume for simplicity that there are no existing generators -- that is, this is a "greenfield" expansion problem where all generators are newly built. 

Finally, assume that any involuntary curtailment of demand caused by shortfalls in supply is penalized at the cost of non-served energy. (Note that by penalizing the opportunity cost of demand curtailment, this cost minimization problem is equivalent to a social welfare maximization problem, assuming inelastic demand).  

#### Problem formulation
This yields the following optimization problem:

```math
\begin{align}
\min &\sum_{g \in G} \left(FixedCost_g \times CAP_g +  \sum_{h \in H} VarCost_g \times GEN_{g,h}\right)& \\
&\quad\quad+ \sum_{h \in H} NSECost \times NSE_h &
\end{align}
```
Such that
```math
\begin{align}
\sum_{g \in G} GEN_{g,h} + NSE_h = Demand_h & \forall \quad h \in H \\
GEN_{g,h} \leq CAP_g & \forall \quad g, h \in G\times H \\
CAP_{g} \geq 0 & \forall \quad g \in G \\
GEN_{g,h} \geq 0 & \forall \quad g, h \in G\times H \\
NSE_{h} \geq 0 & \forall \quad h \in H
\end{align}
```

Where
```math
\begin{align}
&FixedCost_g = Capex_g \times CRF_g + FixedOM_g \\
&CRF_g = \frac{WACC(1+WACC)^{Life}}{(1+WACC)^{Life}-1} \\
&VarCost_g = VarOM_g + HeatRate_g \times FuelCost_g
\end{align}
```

The **decision variables** in the above problem are:

- ``CAP_{g}``, the capacity (in MW) built for each generation type, ``g``; and
- ``GEN_{g,h}``, the generation (in MWh) produced by each generator, ``g``, in each hour, ``h``; and
- ``NSE_h``, the quantity of involuntarily curtailed demand in each hour (in MWh), ``h``.

Note that if we ignore the discrete nature of large thermal power plant decisions (investment and power plant cycling decisions), we have continous decisions and *a linear programming (LP) problem* with linear objective function and linear constraints. (We'll incorporate discrete decisions in the notebook on Unit Commitment)

The **sets** are:
- ``G``, the set of generators: \[geo, coal, CCGT, CT\];
- ``H``, the set of hours in the year: \[1:8760\]

The **parameters** are:

- ``Capex_g``, the capital expenditure to construct the power plant (overnight cost + cost of financing during construction) (\$/MW); 
- ``CRF_g``,  the annual capital recovery factor or share of ``Capex`` recovered in each year to pay off equity and debt (per unit);
- ``WACC_g``,  the weighted average cost of capital to finance the generator, or the weighted cost of interest on debt and returns to equity investors (\%); 
- ``Life_g``, the financial asset life of the asset (years); 
- ``FixedOM_g``, the fixed operations and maintenance cost (\$/MW-year)
- ``VarOM_g``, the variable operations and maintenance cost (\$/MWh);
- ``HeatRate_g``, the rate of fuel consumed per unit of electricity produced (MMBtu/MWh);
- ``FuelCost_g``, the cost of fuel (\$/MMBtu)
- ``NSECost``, the opportunity cost or penalty incurred for involuntary non-served energy (\$/MWh); and
- ``Demand_h``, is the demand in each hour
"""

# ╔═╡ 3e7dd2c2-8404-4f0b-9fe0-19312efbc2d5
md"""
## Building the problem with Julia + JuMP
Now let's define this LP problem with the help of Julia + JuMP.

**1. Load packages**

First, let's load the Julia packages we'll need to work with here."""


# ╔═╡ 119d9ce0-ff01-4fbe-bd33-7572231df32e
md"""

**2. Load parameters (data)**

Then we'll load our parameters, starting with the cost parameters for the candidate generators.

The following generator data are illustrative and approximate, but most are based on the National Renewable Energy Laboratory's Annual Technology Baseline 2020, except for natural gas CT capex, which is based on Lazard's Levelized Cost of Electricity Version 13.0. 
"""

# ╔═╡ 599cb6b0-f671-4fa0-b5c1-5fbd2247707d
 generators = DataFrame(CSV.File("expansion_data/generators_for_expansion.csv")) 

# ╔═╡ 14209440-1e40-463b-acf2-2388c1da21cc
md"""Note we've got wind and solar costs in here, but we'll get to these variable renewable resources in the next section.Next we'll load the hourly demand data. This demand time series is the actual reported demand (from 2019) for Public Service Company of Colorado as reported to FERC Form 714.  This data was extracted from the original CSV files using R.  This code should be moved to Julia and put under version control"""

# ╔═╡ d134e3c1-fbe2-433d-8bc4-7081133b2c98
 demand = DataFrame(CSV.File("expansion_data/psco_2019_demand.csv"))

# ╔═╡ b9e620f1-3db1-4e30-96cf-b7f8019ba6e4
 md"Let's plot the demand to get a feel for what we're working with here, this is all data from 2019 no less!..."

# ╔═╡ eacab643-72cc-488c-905e-455d3d746465
begin
	plot(demand.Demand, label="")
	title!("Hourly Demand")
	xaxis!("Hour")
	yaxis!("MWh")
end

# ╔═╡ 21efcb36-c22a-4594-b7c1-e5a950222e21
md"""
You can see some of the seasonal trends in net electricity demand in the PSCO territory (dominated by the Denver metro area), with some large peaks in demand in the summer months driven by air conditioning needs. Net electricity demand means that this is the demand after the generation of distributed solar PV installed behind the meter, which reduces the net load visible to bulk generators.

The chronological demand profile is a little hard to look at though, with all the daily variation, and since we're not considering intertemporal constraints (yet), let's resort the demand from highest to lowest hour of demand across the year, what is known as a **"load duration curve."**
"""

# ╔═╡ b2fcb5b7-3948-468c-92c4-17c4f99c68c7
begin
	plot(sort(demand.Demand, rev=true), label="")
	title!("Load Duration Curve, PSCO System, CY2019")
	xaxis!("Hour")
	yaxis!("MWh")
end

# ╔═╡ 9feba300-0556-441b-a754-0a2bb28b2fa5
md" Examining the load duration curve, we can more easily see the wide range of variation in hourly demand across the year..."

# ╔═╡ 7afb6f95-b3a6-48f4-a7e3-4daad31cc85d
string("Minimum demand: ",minimum(demand.Demand),"\n")

# ╔═╡ 8da1fae7-a372-42e2-971a-9076215dd65c
string("Maximum demand: ",maximum(demand.Demand),"\n")

# ╔═╡ 0dbe86ff-36fc-4fdf-a0ea-4eab07f3b793
string("Median demand: ",median(demand.Demand),"\n")

# ╔═╡ 0019982d-558f-417e-955b-dc8857578f1e
string("Mean demand: ",round(mean(demand.Demand),digits=0),"\n")

# ╔═╡ 595c0611-3ac5-4db2-9a1e-732631bc06cd
string("Max hour: ",maximum(demand.Hour),"\n")

# ╔═╡ 6295b406-80fc-4027-83ad-719a55a26bc6
md"""
 Finally, we will set the penalty for non-served energy or NSECost to \$9,000/MWh
"""

# ╔═╡ deff61a4-373b-497f-9188-e8a351cba3f7
 NSECost = 9000

# ╔═╡ 6ff61255-77e5-4a0f-895c-bbdf4b88c870
md"""
Now let's define the sets we are going to use to index our variables:
 - ``G``, the set of generators: ``\{geo, coal, CCGT, CT\}``
 - ``H``, the set of hours in the year: ``n \in H \iff 1<n<8760``
"""

# ╔═╡ 5423d127-4ceb-41e6-9303-7a86c874d1a8
# The set of generators from the generators DataFrame
G = generators.G[1:(size(generators,1)-2)];  # note we truncate wind and solar for now

# ╔═╡ fc361eb5-118d-4dc8-a34e-07df4bec5998
# The set of hours in the demand DataFrame
H = demand.Hour;

# ╔═╡ 5e9105a1-cb3e-46c2-9f93-630f0e8d7264
md"""From here we can simply define the model with its decision variables and constraints like so

 - ``CAP_{g} \geq 0``, the capacity (in MW) built for each generation type, ``g``;  
 - ``GEN_{g,h} \geq 0``, the generation (in MWh) produced by each generator, ``g``, in each hour, h
 - ``NSE_h \geq 0``, the quantity of involuntarily curtailed demand in each hour, ``h``.

Our constraints like so

```math
\begin{align}
\sum_{g \in G} GEN_{g,h} + NSE_h = Demand_h & \forall \quad h \in H\\
GEN_{g,h} \leq CAP_g & \forall \quad g \in G \wedge h \in H
\end{align}
```

And our objective function like so

```math
\begin{align}
\min &\sum_{g \in G} \left(FixedCost_g \times CAP_g +  \sum_{h \in H} VarCost_g \times GEN_{g,h}\right)& \\
&\quad\quad+ \sum_{h \in H} NSECost \times NSE_h &
\end{align}
```
"""

# ╔═╡ f9ca5105-5ae6-477e-b66e-c4dc94881078
begin
	#=
	Expansion_Model = Model(Clp.Optimizer)
	@variables(Expansion_Model, begin
	        CAP[g in G] >=0          # Generating capacity built (MW)
	        GEN[g in G, h in H] >= 0 # Generation in each hour (MWh)
	        NSE[h in H] >= 0         # Non-served energy in each hour (MWh)
	end)
	@constraints(Expansion_Model, 
	begin
	    cDemandBalance[h in H], sum(GEN[g,h] for g in G) + NSE[h] == demand.Demand[h]
	    cCapacity[g in G, h in H], GEN[g,h] <= CAP[g]
	end)
	@objective(Expansion_Model, Min,
    sum(generators[generators.G.==g,:FixedCost][1]*CAP[g] + 
        sum(generators[generators.G.==g,:VarCost][1]*GEN[g,h] for h in H)
    for g in G) + 
    sum(NSECost*NSE[h] for h in H) 
)
	=#
end

# ╔═╡ f9d571af-da2a-40bf-8a1e-a5c07eb88f08
md"Then from here we can attempt to solve the model like so"

# ╔═╡ d57130ce-b5a6-4929-b384-a049d8365548
# optimize!(Expansion_Model)

# ╔═╡ 72e33900-0fdd-4838-a3b0-8dddcbb145c2
begin
	#=
	generation = zeros(size(G,1))
	for i in 1:size(G,1) 
		generation[i] = sum(value.(GEN)[G[i],:].data) 
	end
	MWh_share = generation./sum(demand.Demand).*100
	cap_share = value.(CAP).data./maximum(demand.Demand).*100
	results = DataFrame(
		Resource = G, 
		MW = value.(CAP).data,
		Percent_MW = cap_share,
		GWh = generation/1000,
		Percent_GWh = MWh_share
	)
	# Calculate how much non-served energy there was and add to results
	  # The maximum MW of non-served energy is the difference 
	  # between peak demand and total installed generation capacity
	NSE_MW = maximum(value.(NSE).data) 
	  # The total MWh of non-served energy is the difference between
	  # total demand and total generation
	NSE_MWh = sum(value.(NSE).data)
	  # Add or "push" a new row of data to the end of the results DataFrame
	push!(results, ["NSE" NSE_MW NSE_MW/maximum(demand.Demand)*100 NSE_MWh/1000 NSE_MWh/sum(demand.Demand)*100])
	=#
end

# ╔═╡ a71af50a-70de-4152-9333-4e264ea67762
md"Now lettuce add renewables like so, first we import the maximum output for a wind and solar plant depending on the weather conditioins for 2019 like so:"

# ╔═╡ d06cd9c9-f5af-46cf-baac-e82114c20adb
renew= DataFrame(CSV.File("expansion_data/psco_2019_windsun.csv")) 

# ╔═╡ 9b71f1ff-8f78-4de5-8e72-61af50eebbaf
begin
	plot(renew.Wind, label="Wind")
	plot!(renew.Solar, label = "Solar")
	title!("Renewable Energy % of peak Capacity")
	xaxis!("Hour")
	yaxis!("MWh")
end

# ╔═╡ 35de0bf7-f525-4e3f-872c-8b68ea756df0
begin
	dom=1:168
	plot(renew.Wind[dom], label="Wind")
	plot!(renew.Solar[dom], label = "Solar")
	title!("Renewable Energy % of peak Capacity for first week of January")
	xaxis!("Hour")
	yaxis!("MWh")
end

# ╔═╡ ae9c7bf5-4b25-4807-a37d-bb4dd8a0afbc
md"""From earlier, notice how we curtailed renewables Lettuce add them back in like so let us add them back in to our set of generators like so"""

# ╔═╡ c943cbc8-c957-472f-9bc6-d4ad8b3c9c70
Gr=generators.G

# ╔═╡ 1c23e26f-8e54-4dfa-8669-07b97597baa4
begin
	cf=renew
	for fossil in ["Geo","Coal","CCGT","CT"]
		cf[:,fossil]=[1 for h in H]
	end
end

# ╔═╡ 7e65b2f1-07b2-4cdc-ab8c-c247b40d7662
md"generators[5,:FixedCost]*=.1"

# ╔═╡ d6fced85-6fd4-4fb9-89cd-1d1571057d21
names(renew)

# ╔═╡ ab91a70b-3414-432f-8e14-b859a0bf4cff
begin
	#=
	Renewable_Model = Model(Clp.Optimizer)
	@variables(Renewable_Model, begin
	        CAP[g in Gr] >=0          # Generating capacity built (MW)
	        GEN[g in Gr, h in H] >= 0 # Generation in each hour (MWh)
	        NSE[h in H] >= 0         # Non-served energy in each hour (MWh)
	end)
	@constraints(Renewable_Model,
	begin
	    cDemandBalance[h in H], sum(GEN[g,h] for g in Gr) + NSE[h] == demand.Demand[h]
	    cCapacity[g in Gr, h in H], GEN[g,h] <= CAP[g]*cf[h,Symbol(g)]
	end)
	@objective(Renewable_Model, Min,
    sum(generators[generators.G.==g,:FixedCost][1]*CAP[g] + 
        sum(generators[generators.G.==g,:VarCost][1]*GEN[g,h] for h in H) for g in Gr) + 
    sum(NSECost*NSE[h] for h in H) 
)
	=#
end

begin
	cfb=renew
	for fossil in ["Geo","Coal","CCGT","CT","Old_CCGT","Old_CT","Old_Coal","Old_Steam"]
		cfb[:,fossil]=[1 for h in H]
	end
end

function evalBrownfieldModel()
	brown_gens= DataFrame(CSV.File("expansion_data/psco_2019_newgens.csv"))
	Gb=brown_gens.G
	GI=collect(1:length(Gb))
	Brown_Model = Model(Clp.Optimizer)
	@variables(Brown_Model, begin
	        CAP[g in GI] >=0          # Generating capacity built (MW)
	        GEN[g in GI, h in H] >= 0 # Generation in each hour (MWh)
	        NSE[h in H] >= 0          # Non-served energy in each hour (MWh)
	end)
	@constraints(Brown_Model, begin
	    cDemandBalance[h in H], sum(GEN[g,h] for g in GI) + NSE[h] == demand.Demand[h]
	    cCapacity[g in GI, h in H], GEN[g,h] <= CAP[g]*cfb[h,Symbol(Gb[g])]
		cRetirement[g in GI], CAP[g] <= ExCap[g]
	end)
	@objective(Brown_Model, Min, begin
    	sum(brown_gens[g,:FixedCost][1]*CAP[g] + 
        	sum(brown_gens[g,:VarCost][1]*GEN[g,h] for h in H) for g in GI) +
    	sum(NSECost*NSE[h] for h in H)
	end)
end

function SetSCC(df,val)
	nrow=size(df,1)
	newSCC=fill(val,nrow)
	df[:,:SCC]=newSCC
	df[:,:VarCost]=df[:,:VarOM]+df[:,:HeatRate].*df[:,:FuelCost]+df[:,:HeatRate].*df[:,:Emissions].*df[:,:SCC]
end
# ╔═╡ Cell order:
# ╟─8db6b4c4-41c9-4a52-94d0-c4a6ac10a2c4
# ╟─236497a3-ab00-438e-8ad8-50af3c675bdb
# ╟─3e7dd2c2-8404-4f0b-9fe0-19312efbc2d5
# ╠═a053b143-0c44-4743-95b8-bd7dc2cc29eb
# ╟─119d9ce0-ff01-4fbe-bd33-7572231df32e
# ╠═599cb6b0-f671-4fa0-b5c1-5fbd2247707d
# ╟─14209440-1e40-463b-acf2-2388c1da21cc
# ╠═d134e3c1-fbe2-433d-8bc4-7081133b2c98
# ╟─b9e620f1-3db1-4e30-96cf-b7f8019ba6e4
# ╠═eacab643-72cc-488c-905e-455d3d746465
# ╟─21efcb36-c22a-4594-b7c1-e5a950222e21
# ╠═b2fcb5b7-3948-468c-92c4-17c4f99c68c7
# ╟─9feba300-0556-441b-a754-0a2bb28b2fa5
# ╠═7afb6f95-b3a6-48f4-a7e3-4daad31cc85d
# ╠═8da1fae7-a372-42e2-971a-9076215dd65c
# ╠═0dbe86ff-36fc-4fdf-a0ea-4eab07f3b793
# ╠═0019982d-558f-417e-955b-dc8857578f1e
# ╠═595c0611-3ac5-4db2-9a1e-732631bc06cd
# ╟─6295b406-80fc-4027-83ad-719a55a26bc6
# ╠═deff61a4-373b-497f-9188-e8a351cba3f7
# ╟─6ff61255-77e5-4a0f-895c-bbdf4b88c870
# ╠═5423d127-4ceb-41e6-9303-7a86c874d1a8
# ╠═fc361eb5-118d-4dc8-a34e-07df4bec5998
# ╟─5e9105a1-cb3e-46c2-9f93-630f0e8d7264
# ╠═f9ca5105-5ae6-477e-b66e-c4dc94881078
# ╟─f9d571af-da2a-40bf-8a1e-a5c07eb88f08
# ╠═d57130ce-b5a6-4929-b384-a049d8365548
# ╠═72e33900-0fdd-4838-a3b0-8dddcbb145c2
# ╟─a71af50a-70de-4152-9333-4e264ea67762
# ╠═d06cd9c9-f5af-46cf-baac-e82114c20adb
# ╠═9b71f1ff-8f78-4de5-8e72-61af50eebbaf
# ╠═35de0bf7-f525-4e3f-872c-8b68ea756df0
# ╠═ae9c7bf5-4b25-4807-a37d-bb4dd8a0afbc
# ╠═c943cbc8-c957-472f-9bc6-d4ad8b3c9c70
# ╠═1c23e26f-8e54-4dfa-8669-07b97597baa4
# ╠═7e65b2f1-07b2-4cdc-ab8c-c247b40d7662
# ╠═d6fced85-6fd4-4fb9-89cd-1d1571057d21
# ╠═ab91a70b-3414-432f-8e14-b859a0bf4cff
# ╠═b255d5b7-4624-4429-8a04-bcd75b207764
# ╠═075714fb-fa0e-425b-981a-f946e4a7813b
# ╠═2064cd02-24f7-4c0b-8c93-f3c28618e6bb
