### A Pluto.jl notebook ###
# v0.14.7

using Markdown
using InteractiveUtils

# ╔═╡ 418ce29a-be2c-11eb-2c2e-712934ad209b
begin
	using JuMP, GLPK
	using Plots; plotly();
	using VegaLite  # to make some nice plots
	using DataFrames, CSV, PrettyTables
	ENV["COLUMNS"]=120; # Set so all columns of DataFrames and Matrices are displayed
end

# ╔═╡ 40e7c5c5-cbf8-4e41-85a7-737142166eae
md"From here we first initalise our data like so"

# ╔═╡ f191a08d-5ba1-46e2-af64-b321ee8690ec
begin
	datadir = joinpath("ed_data") 
	# Note: joinpath is a good way to create path reference that is agnostic
	# to what file system you are using (e.g. whether directories are denoted 
	# with a forward or backwards slash).
	gen_info = CSV.read(joinpath(datadir,"Generators_data.csv"), DataFrame);
	fuels = CSV.read(joinpath(datadir,"Fuels_data.csv"), DataFrame);
	loads = CSV.read(joinpath(datadir,"Demand.csv"), DataFrame);
	gen_variable = CSV.read(joinpath(datadir,"Generators_variability.csv"), DataFrame);
	
	# Rename all columns to lowercase (by convention)
	for f in [gen_info, fuels, loads, gen_variable]
	    rename!(f,lowercase.(names(f)))
	end
end

# ╔═╡ acdf8fda-3825-41ff-bd71-f8948c458b23
md"Then construct the generator platform"

# ╔═╡ db8de567-b6e6-4736-bd29-1f31ad076b2c
gen_info

# ╔═╡ fb79b84c-61c6-44fd-9cdc-cb62f5a26d89


# ╔═╡ Cell order:
# ╠═418ce29a-be2c-11eb-2c2e-712934ad209b
# ╠═40e7c5c5-cbf8-4e41-85a7-737142166eae
# ╠═f191a08d-5ba1-46e2-af64-b321ee8690ec
# ╠═acdf8fda-3825-41ff-bd71-f8948c458b23
# ╠═db8de567-b6e6-4736-bd29-1f31ad076b2c
# ╠═fb79b84c-61c6-44fd-9cdc-cb62f5a26d89
