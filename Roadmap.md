---
title: Roadmap
author:  Bradley & Nic Venner
---

# 28 May 2021

## Repository

- Be able to import model as a Julia package

- Come up with a cool model name.  

- Working name: corvid

## Performance

- Be able to run parallel model fits using *pmap* in multiple threads

- Develop automated model workflow: Data -> Scenario -> Model -> Results

## Scenarios

- Specify scenarios in a Julia file.  Scenarios mediate between *Data* and *Models*.  They include important aspects of the problem domain, including variation in important parameters such as the social cost of carbon.

- Should scenarios be distinct from JuMP, or should mapping from the scenario to JuMP be done in *Models*?   ,

- Since PowerGenome exports to a GenX input file, could we make scenarios compatible with GenX?

## Data

- The *Data* folder/respository should include 'raw' data obtained from Internet sources

- As much as possible, write ETL workflows in Julia.  Build as much as possible on PowerGenome/PUDL.

- Should data be a folder or a submodule?  Data might be used by other workgroups. 

## Models

- Run multi-year models.

- Allow for time-varying prices, both within and between years.

- Use *Unitful* consistently throughout the models.  This includes being able to import data from a database that has units as part of its schema.

- Design model 'input' as a scenario that can be passed to JuMP

- Think about how to specify deterministic models to that they can be adapted to stochastic optimization
