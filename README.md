Analysis of the 2021 Clean Energy Plan by Public Service Company of Colorado
-

[![License: CC BY 4.0](https://img.shields.io/badge/License-CC%20BY%204.0-lightgrey.svg)](https://creativecommons.org/licenses/by/4.0/) [![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

These materials are heavily adapted from course materials that were jointly developed by [Michael Davidson](http://mdavidson.org/) and [Jesse Jenkins](https://mae.princeton.edu/people/faculty/jenkins) for introducing constrained optimization models applied to power systems. 

### Description

This project will apply the course materials developed by Michael Davidson and Jesse Jenkins to specific issues related to the Clean Energy Plan (CEP) developed by Public Service Company of Colorado (PSCO) in 2021.  

The clean energy plan was developed due to requirements passed by the Colorado State Legislature in 2019 in HB19-1261 and SB19-236.  The former bill required 50% greenhouse gas emissions cuts by 2030, and the latter required utilities to cut emissions by 80%, both relative to a 2005 baseline.  A2


Our analysis of the Clean Energy Plan follows the Davidson & Jenkins course structure, begining with analyses of some of the basic capacity expansion problems considered by the CEP. It then considers the operational modeling problems of economic dispatch, unit commitment, and optimal network power flow.  These operational models are then used in a "complex" capacity expansion plan. If things go well, this last plan will be able to be used to compare with the PSCO

Finally, several datasets of realistic power systems are provided which students will use in conjunction with building a model for a course project that answers a specific power systems question.

### Notebooks

1. [Constrained Optimization](Notebooks/01-Constrained-Optimization.ipynb)

2. [Using Julia and JuMP for Constrained Optimization](Notebooks/02-Anatomy-of-a-Model.ipynb)

3. [Basic Capacity Expansion Planning](Notebooks/03-Basic-Capacity-Expansion.ipynb)

4. [Economic Dispatch](Notebooks/04-Economic-Dispatch.ipynb)

5. [Unit Commitment](Notebooks/05-Unit-Commitment.ipynb)

6. [DC Optimal Network Power Flow](Notebooks/06-Optimal-Power-Flow.ipynb)

7. [Complex Capacity Expansion Planning](Notebooks/07-Complex-Capacity-Expansion.ipynb)

### Homeworks

1. [Homework 1 - Building Your First Model](Homeworks/Homework-01.ipynb)

2. [Homework 2 - Basic Capacity Expansion](Homeworks/Homework-02.ipynb)

3. [Homework 3 - Unit Commitment](Homeworks/Homework-03.ipynb)

4. [Homework 4 - Optimal Power Flow](Homeworks/Homework-04.ipynb)

5. [Homework 5 - Complex Capacity Expansion](Homeworks/Homework-05.ipynb)

### Project

[Project dataset descriptions](Project/)

1. [ERCOT 120-bus 500kV simulated system for optimal power flow and economic dispatch problems](Project/ercot_500kV/)

2. [ERCOT 3-zone 2040 brownfield expansion system for capacity expansion planning problems](Project/ercot_brownfield_expansion) - (See [Notebook 7](Notebooks/07-Complex-Capacity-Expansion.ipynb) for description)

3. [WECC 6-zone 2045 brownfield expansion system w/100% clean resources for capacity planning problems](Project/wecc_2045_all_clean_expansion)

4. [WECC 12-zone 2020 current capacity for unit commitment and economic dispatch problems](Project/wecc_2020_unit_commitment)

### License and copyright

If you would like to use these materials, please see the [license and copyright page](LICENSE.md).


